from django.urls import path, include
from rest_framework.routers import DefaultRouter

from shorteners.views import *


router = DefaultRouter()

router.register(r'urls', UrlShortenerViewSet, basename='urls')


urlpatterns = [
    path('', include(router.urls)),
    path('r/<str:short_url>', RedirectView.as_view()),
]

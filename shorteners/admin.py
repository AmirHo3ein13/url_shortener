from django.contrib import admin

from .models import *

admin.site.register(Url)
admin.site.register(Visit)

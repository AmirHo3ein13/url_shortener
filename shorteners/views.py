import datetime
import pickle

import redis
from django.conf import settings
from django.shortcuts import redirect
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import viewsets, mixins

from shorteners.models import Url
from shorteners.serializers import UrlSerializer, UrlStatsSerializer
from shorteners.tasks import CacheVisitTask


redis_instance = redis.StrictRedis.from_url(settings.CELERY_BROKER_URL)


class RedirectView(APIView):
    def get(self, request, short_url, *args, **kwargs):
        url = redis_instance.get(short_url)
        if url:
            url = pickle.loads(url)
        else:
            url = Url.objects.get(shortened=short_url)
            redis_instance.set(short_url, pickle.dumps(url), ex=2 * 60 * 60)

        CacheVisitTask.delay(url.id,
                             self.request.META.get('HTTP_X_FORWARDED_FOR'),
                             self.request.META.get('REMOTE_ADDR'),
                             self.request.META.get('HTTP_USER_AGENT'),
                             datetime.datetime.now())

        return redirect(url.url)


class UrlShortenerViewSet(viewsets.GenericViewSet,
                          mixins.CreateModelMixin,
                          mixins.ListModelMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = UrlSerializer

    def get_queryset(self):
        return Url.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return UrlStatsSerializer
        elif self.action == 'create':
            return UrlSerializer

import random
import string
from datetime import timedelta

from django.db import models
from django.utils.functional import cached_property
from django.utils.timezone import localdate

from users.models import User


SHORTENED_LENGTH = 6


def random_string(length):
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


class Url(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    url = models.CharField(max_length=100)
    shortened = models.CharField(max_length=SHORTENED_LENGTH, unique=True, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.pk and not self.shortened:
            self.shortened = random_string(SHORTENED_LENGTH)
        if not self.url.startswith('http://') and not self.url.startswith('https://'):
            self.url = f'http://{self.url}'
        super(Url, self).save()

    @cached_property
    def last_31_days_visits(self):
        """
        get query set filtered for last 31 days (including today)
        :return:
        """
        return self.visit_set.filter(created_at__gte=localdate() - timedelta(days=31))


class Visit(models.Model):
    __DEVICE_TYPE = (('desktop', 'Desktop'), ('mobile', 'Mobile'))

    url = models.ForeignKey(Url, on_delete=models.CASCADE)
    ip = models.CharField(max_length=15)
    browser = models.CharField(max_length=150)
    device = models.CharField(max_length=7, choices=__DEVICE_TYPE)
    created_at = models.DateTimeField(auto_now_add=True)

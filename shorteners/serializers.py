from datetime import date, datetime, timedelta

from django.db.models import Count
from django.utils import timezone
from django.utils.timezone import localdate
from rest_framework import serializers

from shorteners.models import Url, SHORTENED_LENGTH


class UrlSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    shortened = serializers.CharField(max_length=SHORTENED_LENGTH, allow_null=True, required=False)

    class Meta:
        model = Url
        fields = ('user', 'shortened', 'url')


class UrlStats:
    def __init__(self, queryset, distinct=False):
        self.queryset = queryset
        self.distinct = distinct

    def __prepare_response(self, queryset):
        return {
            'total': queryset.count(),
            'device': queryset.values('device').annotate(count=Count('ip', distinct=self.distinct)).all(),
            'browser': queryset.values('browser').annotate(count=Count('ip', distinct=self.distinct)).all()
        }

    def __get_today_visits(self):
        """
        get query set for today's visits
        :return:
        """
        today = self.queryset.filter(created_at__date=localdate())
        return self.__prepare_response(today)

    def __get_yesterday_visits(self):
        """
        get query set for yesterday's visits
        :return:
        """
        yesterday = self.queryset.filter(created_at__date=localdate() - timedelta(days=1))
        return self.__prepare_response(yesterday)

    def __get_last_7_days_visits(self):
        """
        get query set for last 7 days' visits
        :return:
        """
        last_7_days = self.queryset.filter(created_at__gte=timezone.now() - timedelta(days=7),
                                           created_at__lt=timezone.make_aware(datetime.combine(date.today(),
                                                                                               datetime.min.time())))
        return self.__prepare_response(last_7_days)

    def __get_last_30_days_visits(self):
        """
        get query set for last 30 days' visits
        :return:
        """
        return self.__prepare_response(self.queryset)

    def get_response(self):
        return {
            'today_visit': self.__get_today_visits(),
            'yesterday_visits': self.__get_yesterday_visits(),
            'last_7_days_visits': self.__get_last_7_days_visits(),
            'last_30_days_visits': self.__get_last_30_days_visits()
        }


class UrlStatsSerializer(serializers.ModelSerializer):
    distinct_users_stats = serializers.SerializerMethodField()
    users_stats = serializers.SerializerMethodField()

    class Meta:
        model = Url
        exclude = ('user',)

    def get_distinct_users_stats(self, obj: Url):
        """
        get stats for distinct users
        :param obj: obj passed to the UrlStats class
        :return:
        """
        return UrlStats(obj.visit_set.values('ip').distinct(), True).get_response()

    def get_users_stats(self, obj: Url):
        """
        get stats for all users
        :param obj: obj passed to the UrlStats class
        :return:
        """
        return UrlStats(obj.visit_set).get_response()

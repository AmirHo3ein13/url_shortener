import datetime

import celery
from django.conf import settings

from shorteners.models import Visit
from shorteners.redis_queue import RedisQueue
from url_shortener.celery import app


@app.task
def visit_insertion():
    """
    celery task to get Visit objects from the redis queue and put them in the DB together
    :return:
    """
    queue = RedisQueue(settings.REDIS_QUEUE_NAME)

    visits = list()
    for _ in range(100):
        visit = queue.get_nowait()
        if not visit:
            break
        visits.append(visit)

    Visit.objects.bulk_create(visits)


class CacheVisit(celery.Task):
    """
    celery task to put Visit object in redis queue
    """
    def __get_client_ip(self, http_x_forwarded_for, remote_addr):
        """
        find client ip from request parameters
        :param http_x_forwarded_for header of request
        :param remote_addr header of request
        :return:
        """
        if http_x_forwarded_for:
            ip = http_x_forwarded_for.split(',')[0]
        else:
            ip = remote_addr
        return ip

    def run(self, url_id, http_x_forwarded_for, remote_addr, http_user_agent, time: datetime.datetime, *args, **kwargs):
        visit = Visit(
            url_id=url_id,
            ip=self.__get_client_ip(http_x_forwarded_for, remote_addr),
            browser=http_user_agent,
            device='mobile' if 'mobile' in http_user_agent.lower() else 'desktop',
            created_at=time
        )

        RedisQueue(settings.REDIS_QUEUE_NAME).put(visit)


CacheVisitTask = app.register_task(CacheVisit())

import os
from url_shortener.celery import app
from celery.schedules import crontab

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'yektanet2',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": 'redis://127.0.0.1:6379/1',
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
        "KEY_PREFIX": "url_shortener"
    }
}

# Allowed Hosts
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '0.0.0.0']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'sxbzx^@%obq(#$tbx!soum@&+9)ibwnme*kjad4=6)%l4he%)*'

CELERY_BROKER_URL = 'redis://localhost:6379/1'

CELERY_IMPORTS = ['shorteners.tasks']


def parse_cron_tab(cron_format, default):
    try:
        parts = cron_format.split()
        if len(parts) < 5:
            return default
        return crontab(minute=parts[0], hour=parts[1], day_of_week=parts[2],
                       day_of_month=parts[3], month_of_year=parts[4])
    except:
        return default


app.conf.beat_schedule = {
    'periodic_visit_insertion': {
        'task': 'shorteners.tasks.visit_insertion',
        'schedule': parse_cron_tab(os.environ.get('PERIODIC_VISIT_INSERTION'), 10),
        'args': ()
    },
}
